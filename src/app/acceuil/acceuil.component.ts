import { Component, OnInit } from '@angular/core';
import { UsrService } from '../services/usr.service';
import { Account, AccountImpl } from '../model/Account';

@Component({
  selector: 'app-acceuil',
  templateUrl: './acceuil.component.html',
  styleUrls: ['./acceuil.component.css']
})
export class AcceuilComponent implements OnInit{

  users: AccountImpl[] = []

  constructor(private usrService: UsrService){}

  ngOnInit(): void {
      this.usrService.getUser().subscribe(x => this.users = x)
  }

}
